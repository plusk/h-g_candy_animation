ToonBoomAnimationInc PaletteFile 2
Solid    Outlines                   0x0850d2753e200b5c   0   0   0 255
Solid    White                      0x0850d2753e200b5e 255 255 255 255
Solid    Red                        0x0850d2753e200b60 255   0   0 255
Solid    Green                      0x0850d2753e200b62   0 255   0 255
Solid    "Vectorized Line"          0x0000000000000003   0   0   0 255
Solid    Blue                       0x0850d2753e200b64   0   0 255 255
Solid    Cupcake_base_fill          0x08525bb9e59740df 210 134 124 255
Solid    Cupcake_base_shade         0x08525bb9e599e31d 173 105  98 255
Solid    Cupcake_top_shade          0x08525bb9e599128e 116 161 179 255
Solid    Cupcake_top_fill           0x08525bb9e5975e74 144 196 208 255
Solid    Cupcake_top_highlight      0x08525bb9e59a1f72 177 221 230 255
Solid    jellybean-fill             0x08525bb9e59c10e2 255 151  17 255
Solid    jellybean_shade            0x08525bb9e59ca930 186 127  57 255
Solid    jellybean_highlight        0x08525bb9e59cf4cd 251 252 130 255
Solid    "New 8"                    0x08528b54e1730a47 202 114 129 255
Solid    "New 9"                    0x08528b54e174261c 247 139 159 255
Solid    "New 10"                   0x08528b54e1743da4 252 202 202 255
Solid    "New 11"                   0x08528b54e174c3b9 159  78  78 255
