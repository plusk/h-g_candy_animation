ToonBoomAnimationInc PaletteFile 2
Solid    outlines                   0x085125f42f100a6e   0   0   0 255
Solid    White                      0x085125f42f100a70 255 255 255 255
Solid    Red                        0x085125f42f100a72 255   0   0 255
Solid    Green                      0x085125f42f100a74   0 255   0 255
Solid    Blue                       0x085125f42f100a76   0   0 255 255
Solid    "Vectorized Line"          0x0000000000000003   0   0   0 255
Solid    lollipop-highlight         0x085176ddef96e950 237 155 155 255
Solid    lollipop-fill              0x085176ddef971b88 204 122 122 255
Solid    lollipop-shade             0x085176ddef973b0a 170  89  89 255
Solid    lollipop-highlight2        0x085176ddef9748fb 247 203 203 255
Solid    lollipop-basefill          0x085176ddef9b7ca6 166 150 101 255
Solid    lollipop-baseshade         0x085176ddef9bf369 121 109  71 255
Solid    lollipop-base-highlight    0x085176ddef9c3c7c 238 212 133 255
Solid    gumdrop-fill               0x08529489276296a9  62 171  54 255
Solid    gumdrop-shade              0x085294892762bbd8  34 125  27 255
Solid    gumdrop-highlight          0x0852948927632d29 188 249 139 255
Solid    "New 0"                    0x0852cde9372be23d 218 143  54 255
Solid    "New 1"                    0x0852cde9372d1894 178 116  43 255
Solid    "New 2"                    0x0852cde9372d5d37 237 220  76 255
Solid    "New 3"                    0x0852cde9372d95e8 255 254  97 255
