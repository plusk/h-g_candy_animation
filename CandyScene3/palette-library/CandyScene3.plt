ToonBoomAnimationInc PaletteFile 2
Solid    Black                      0x08513e4535200acd   0   0   0 255
Solid    White                      0x08513e4535200acf 255 255 255 255
Solid    Red                        0x08513e4535200ad1 255   0   0 255
Solid    Green                      0x08513e4535200ad3   0 255   0 255
Solid    Blue                       0x08513e4535200ad5   0   0 255 255
Solid    "Vectorized Line"          0x0000000000000003   0   0   0 255
Solid    cupcake-base-fill          0x08528b54e17a0e5b 186 148  92 255
Solid    cupcake-top-highlight      0x08528b54e17a2a70 241 162 162 255
Solid    cupcake-cherry             0x08528b54e17bf023 163  39  39 255
Solid    cupcake-top-shade          0x08528b54e17c397d 208 121 121 255
Solid    cupcake-base-shade         0x08528b54e17cb96d 153 121  75 255
Solid    "New 5"                    0x08528b54e17d7872 245 192 192 255
Solid    cookie-fill                0x08528b54e17fd178 189 148  90 255
Solid    cookie-spot1               0x08528b54e17fe92b 234 116 233 255
Solid    cookie-spot2               0x0852948927602bc3 253 235  79 255
Solid    cookie-spot3               0x0852948927607747  86 248 117 255
Solid    "New 0"                    0x0852cde937261af5 205 110 110 255
Solid    "New 1"                    0x0852cde9372784b3 238 158 158 255
Solid    "New 2"                    0x0852cde93727cb99 178  85  85 255
Solid    "New 3"                    0x0852cde93728002e  99  35 103 255
